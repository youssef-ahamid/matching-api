import os
import spacy
from fastapi import FastAPI, HTTPException
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from typing import List, Dict
from pydantic import BaseModel

app = FastAPI()

# load the spaCy model and the sentiment analyzer
nlp = spacy.load("en_core_web_sm")
sent_analyzer = SentimentIntensityAnalyzer()


class Answer(BaseModel):
    id: str
    label: str
    value: str


class Response(BaseModel):
    id: str
    userId: str
    answers: List[Answer] = []


def get_answer_values(response: Response) -> Dict[str, str]:
    answer_dict = {}
    for answer in response.answers:
        answer_dict[answer.label] = answer.value
    return answer_dict


def sent_sim_score(user1vals, user2vals):
    if isinstance(user1vals, (str, int, float, bool)):
        if isinstance(user2vals, (str, int, float, bool)):
            # if both values are strings, calculate the semantic similarity between them using spaCy
            doc1 = nlp(str(user1vals))
            doc2 = nlp(str(user2vals))
            score = doc1.similarity(doc2)
            # if(score>0.8):
            #   exact_matches.append
            # total_score += score
            # num_scores += 1

            # calculate the sentiment scores for the two values using NLTK's SentimentIntensityAnalyzer
            sentiment1 = sent_analyzer.polarity_scores(str(user1vals))["compound"]
            sentiment2 = sent_analyzer.polarity_scores(str(user2vals))["compound"]
            sent_score = abs(sentiment1 - sentiment2)
            return score, sent_score
        elif isinstance(user2vals, list):
            # if one value is a string and the other is a list, join the list items into a single string
            val1_str = str(user1vals)
            val2_str = ' '.join([str(elem) for elem in user2vals])

            # calculate the semantic similarity between the two strings using spaCy
            doc1 = nlp(val1_str)
            doc2 = nlp(val2_str)
            score = doc1.similarity(doc2)

            # calculate the sentiment scores for the two strings using NLTK's SentimentIntensityAnalyzer
            sentiment1 = sent_analyzer.polarity_scores(val1_str)["compound"]
            sentiment2 = sent_analyzer.polarity_scores(val2_str)["compound"]
            sent_score = abs(sentiment1 - sentiment2)
            return score, sent_score
    elif isinstance(user1vals, list):
        if isinstance(user2vals, list):
            # if both values are lists, join the list items into two separate strings
            val1_str = ' '.join([str(elem) for elem in user1vals])
            val2_str = ' '.join([str(elem) for elem in user2vals])

            # calculate the semantic similarity between the two strings using spaCy
            doc1 = nlp(val1_str)
            doc2 = nlp(val2_str)

            score = doc1.similarity(doc2)

            # calculate the sentiment scores for the two strings using NLTK's SentimentIntensityAnalyzer
            sentiment1 = sent_analyzer.polarity_scores(val1_str)["compound"]
            sentiment2 = sent_analyzer.polarity_scores(val2_str)["compound"]
            sent_score = abs(sentiment1 - sentiment2)
            return score, sent_score


def calculate_similarity(user1, user2):
    # initialize the similarity score and sentiment score to 0
    similarity_score = 0
    sentiment_score = 0
    preferences_factor = 1
    matched_on = []
    # rejected_factor = 0.00000001
    # iterate over the keys in the user1 dictionary
    for key in user1.keys():
        if key in ["name", "age", "languages", "email", "slogan", "reddit_username", "contact_info", "gender",
                   "ice_breakers", "welcome_message"]:
            continue
        if key in ["communication_frequency", "communication_preferences", "voice_chat_preference"]:
            continue
        if key in ["music_to_share", "languages_spoken", "musical_instruments_played", "zodiac_sign"]:
            continue
        if key in ["match_with_instrument_players"]:
            user1_values = user1.get(key, [])
            user2_values = user2.get("musical_instruments_played", [])
            keySim, keySent = sent_sim_score(user1_values, user2_values)
            if keySim > 0.75:
                preferences_factor += 0.05
                matched_on.append(key)
            continue
        if key in ["match_with_people_who_play"]:
            user1_values = user1.get(key, [])
            user2_values = user2.get("games_currently_playing", [])
            keySim, keySent = sent_sim_score(user1_values, user2_values)
            if keySim > 0.75:
                preferences_factor += 0.05
                matched_on.append(key)
            continue
        if key in ["gender_preferences"]:
            if user1["gender"] == 'Male':
                if 'Female' in user1["gender_preferences"]:
                    if user2["gender"] == 'Female':
                        # rejected_factor+=0.000000001
                        return 0, matched_on
            if user1["gender"] == 'Female':
                if 'Male' in user1["gender_preferences"]:
                    if user2["gender"] == 'Male':
                        # rejected_factor+=0.000000001
                        return 0, matched_on
            continue
        if key in ["looking_for_zodiac_sign"]:
            user1_values = user1.get(key, [])
            user2_values = user2.get("zodiac_sign", [])
            keySim, keySent = sent_sim_score(user1_values, user2_values)
            if keySim > 0.75:
                preferences_factor += 0.05
                matched_on.append(key)
            continue
        if key in ["mbti_preferences"]:
            user1_values = user1.get(key, [])
            user2_values = user2.get("personality_type", [])
            keySim, keySent = sent_sim_score(user1_values, user2_values)
            if keySim > 0.75:
                preferences_factor += 0.05
                matched_on.append(key)
            continue
        # get the values for the current key from both users
        user1_values = user1.get(key, [])
        user2_values = user2.get(key, [])
        KeySim, KeySent = sent_sim_score(user1_values, user2_values)
        if KeySim > 0.75:
            preferences_factor += 0.05
            matched_on.append(key)
            # check if the values are strings, integers, floats, or booleans
        sentiment_score += KeySent
        similarity_score += KeySim

    # divide the overall scores by the number of scores to get the

    # divide the overall score by the number of keys to get the average similarity score
    num_keys = len(user1.keys())
    average_similarity_score = similarity_score / num_keys
    # average_sentiment_score = sentiment_score / num_keys
    average_similarity_score *= preferences_factor
    return average_similarity_score, matched_on


def max_indices(arr):
    max_indices_arr = []
    for i in range(3):
        max_index = arr.index(max(arr))
        max_indices_arr.append(max_index)
        arr[max_index] = -float('inf')
    return max_indices_arr


def find_best_match(response, responses):
    best_matches = []
    best_scores = []
    matched_on_acc = []
    output_matches = []
    output_matched_on = []
    responses_ids = []
    user_dict = get_answer_values(response)
    for u in responses:
        if u != response:
            user2_dict = get_answer_values(u)
            score, matched_on = calculate_similarity(user_dict, user2_dict)
            best_matches.append(u.id)
            best_scores.append(score)
            matched_on_acc.append(matched_on)
            responses_ids.append(u.id)
    indices = max_indices(best_scores)
    for i in indices:
        output_matches.append(best_matches[i])
        output_matched_on.append(matched_on_acc[i])
    return output_matches, output_matched_on


@app.post("/best-matches")
def get_best_matches(user: Response, users: List[Response]):
    try:
        best_matches, matched_on = find_best_match(user, users)
        response = {"best_matches": best_matches, "matched_on": matched_on}
        return response
    except ValueError as e:
        raise HTTPException(status_code=400, detail=str(e))


if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=int(os.environ.get("PORT", 8000)))
